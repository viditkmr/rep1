var toastEvent = $A.get("e.force:showToast");
toastEvent.setParams({
	"type":"error",
	"title": "Error!",
	"message": "Error during deletion of Manufacturing Run."
});
toastEvent.fire(); 
return;

var toastEvent = $A.get("e.force:showToast");
toastEvent.setParams({
	"type":"error",
	"title": "Success!",
	"message": "Error during deletion of Manufacturing Run."
});
toastEvent.fire(); 
return;


var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type":"warning",
                        "title": "Warning!",
                        "mode": "pester",
                        "message": 'Please select any transaction to print !'
                    });
                    toastEvent.fire(); 
                    return;

//Spinner toggle
<div class="slds-hide" aura:id="spinner">
        <lightning:spinner style="position: fixed" alternativeText="Loading" size="large" />
    </div>

var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'slds-hide');
$A.util.toggleClass(spinner, "slds-hide");


//style when using ui:outputText as not editable field
<div class="slds-form-element__label">Quote Type</div>
<div class="slds-box slds-box_x-small slds-form-element__control slds-grow">
	<ui:outputText value="{!v.companyName}"/>
</div>


//to go sobject
window.location = "/"+quoteId;

//lookup filter for already selected record ids
<c:customLookUp recordID="{!v.qtlpro.Product2Id}" recordName="{!v.prodName}" objectAPIName="product2" whereCondition="{!v.proFilter? 'AND Id NOT IN ('+v.proFilter+') AND Id IN (select Product2Id from PricebookEntry where Pricebook2.name = \'Standard Price Book\' )':'AND Id IN (select Product2Id from PricebookEntry where Pricebook2.name = \'Standard Price Book\' )'}" IconName="standard:product" label=""/>

to add filter
var profilter = component.get("v.productFilter");
                if(profilter != "")
                    profilter = profilter+',';
                profilter = profilter + "'"+objectAPIName+"'";
                component.set("v.productFilter",profilter);

//to check lightning or classic in javascript and redirect to record page
closeCancelModal : function(component, event, helper) 
    {
        var device = $A.get("$Browser.formFactor");
        if( (typeof sforce != 'undefined') && sforce && (!!sforce.one) ) 
        {
            sforce.one.navigateToSObject( component.get("v.recordId") ); 
        }
        else 
        {
            var url = window.location.href;
            var value = url.substr(0,url.lastIndexOf('apex') ) + component.get("v.recordId");  
            window.location.replace(value);
        }
    },

//for bitbucket commit from terminal